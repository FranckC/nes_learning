-- Lancer la rom, puis lancer une savestate
-- dans une instance de jeu où le vaisseau vit.
-- Puis, lancer le GUI. Puis enjoy.

-- Vitesse de l'émulation ("turbo" ou "normal")
emu.speedmode("turbo");

-- Ouverture du fichier d'enregistrement de la run
best_frame=io.open('best_frame.input','w');

-- Initialisation des savestates
slot1=savestate.create(3);
slot2=savestate.create(2);

-- paramètres de l'environnement
frame_max = 2000000;
x_max = 15000;
y_max = 18000;
action_max = 5;
input_frame_space=12;
segment_inputs=200;
caution_margin=2;
shoot_framerate=12;

segment_frames=input_frame_space*segment_inputs;

-- On enregistre le paramètre input_frame_space
best_frame:write(input_frame_space);
best_frame:write('\n');

-- Best policy at current iteration
policy = {};

-- Initialisation d'un tableau de décompte de visite des états
visit_counter={};

-- Initialisation de la Q-value
q_value={};

-- Initialisation des historique d'input
input_seq_new={};
input_seq_old={};

-- Initialisation des save et des frame
savestate.save(slot1);
savestate.save(slot2);
last_frame_save=emu.framecount();
backup_frame_save=last_frame_save;
frame_save1=last_frame_save;
frame_save2=last_frame_save;
frame_alive=last_frame_save;
last_is_1=true;

while true do
	-- Est-on mort trop vite à partir de là ?
	-- (repère une mauvaise sauvegarde et load celle d'avant)
	dead_fast=(frame_alive-last_frame_save-input_frame_space)<(segment_frames/caution_margin);
	
	if dead_fast then
		cautious_slot=not last_is_1;
		input_seq_old={};
	else
		cautious_slot=last_is_1;
	end
	input_seq_new={};
				
	-- Chargement de la bonne sauvegarde
	if cautious_slot then
		savestate.load(slot1);
		savestate.save(slot1);
		frame_alive=frame_save1;
	else
		savestate.load(slot2);
		savestate.save(slot2);
		frame_alive=frame_save2;
	end;
		
	isAlive=true;
		
	-- État initial:
	vic_x_i=memory.readword(0x00102C94);
	vic_y_i=memory.readword(0x00102C92);
	state_i=frame_alive+frame_max*(vic_x_i+x_max*vic_y_i);	

	-- Tant que le vaisseau n'est pas détruit
	while isAlive do
			
		-- Si on a avancé d'assez de frames, sauvegarder
		if (frame_alive-last_frame_save)>(segment_frames-1) then
			if last_is_1 then
				savestate.save(slot2);
				frame_save2=frame_alive;
				last_frame_save=frame_save2;
				backup_frame_save=frame_save1;
			else
				savestate.save(slot1); 
				frame_save1=frame_alive; 
				last_frame_save=frame_save1; 
				backup_frame_save=frame_save2;
			end
			
			last_is_1 = not last_is_1;
			
			-- Enregistrer les inputs désormais certains			
			for i,v in ipairs(input_seq_old) do
				best_frame:write(v);
				best_frame:write('\n');
			end
			best_frame:flush();
						
			input_seq_old = input_seq_new;
			input_seq_new={};
		end;

		-- Corriger une mauvaise sauvegarde
		if (deadfast 
			and ((frame_alive-backup_frame_save)>(segment_frames-1)) 
			and (frame_alive-backup_frame_save)<(segment_frames+input_frame_space))
		then
			if last_is_1 then
				savestate.save(slot1);
				frame_save1=frame_alive;
				last_frame_save=frame_save1;
			else
				savestate.save(slot2);
				frame_save2=frame_alive;
				last_frame_save=frame_save2;
			end
		end
				
		-- Sélectionner l'action à cette étape
		action=0;
		if policy[state_i] then
			action = policy[state_i];
		end
			
		if frame_alive < last_frame_save then
			table.insert(input_seq_old,action);
		else
			table.insert(input_seq_new,action);
		end
		
		-- calcul de l'indice dans l'espace state-action
		state_action=frame_alive+frame_max*(vic_x_i+x_max*(vic_y_i+y_max*action));

		-- Mettre à jour le compteur de visite
		if visit_counter[state_action] then
			visit_counter[state_action]=
				visit_counter[state_action]+1;			
		else
			visit_counter[state_action]=1;
		end
		
		a=visit_counter[state_action];
		
		-- Définition des inputs à cette frame
		if action==0 then
			inputs_shoot={["P1 Button 1"]=true};
			for frame=1,input_frame_space,1 do
				if ((frame%shoot_framerate)<6) then
					joypad.set(inputs_shoot)
				end
				emu.frameadvance();
			end
		else
			go_up= (action==1)or(action==2);	
			go_left=(action==1)or(action==3);

			-- Mise sous forme acceptée par l'émulateur
			inputs_noshoot={["P1 Up"]=go_up, ["P1 Down"]=not go_up,
				["P1 Left"]=go_left, ["P1 Right"]=not go_left
				};
			inputs_shoot={["P1 Up"]= go_up, ["P1 Down"]= not go_up,
				["P1 Left"]=go_left, ["P1 Right"]=not go_left,
				["P1 Button 1"]=true
				};
				
			-- Saisie des inputs pendant x frames;
			for frame=1,input_frame_space,1 do
				if ((frame%shoot_framerate)<6) then
					joypad.set(inputs_shoot);
				else
					joypad.set(inputs_noshoot);
				end
				emu.frameadvance();
			end
				
		end
				
		-- Récupération du nouvel état
		new_frame=emu.framecount();
		vic_x=memory.readword(0x00102C94);
		vic_y=memory.readword(0x00102C92);
		state=new_frame+frame_max*(vic_x+x_max*vic_y);

		-- Vérifier qu'on est pas mort
		isAlive=memory.readbyte(0x00102D16)==1;
		
		-- Définition de la reward
		R = 0;
		if not isAlive then
			R = -1;
		end

		-- max_a Q(X_t+1,a)
		max_q_value = -math.huge;
		for new_action=0,(action_max-1),1 do
			new_state_action=new_frame+
				frame_max*(vic_x+
				x_max*(vic_y+
				y_max*new_action));
			if q_value[new_state_action] then
				new_q_value=q_value[new_state_action] 
				if new_q_value > max_q_value then
					max_q_value=new_q_value;
				end
			else 
				max_q_value = 0;
			end
			
		end

		-- Initialisation de la Q-value si besoin
		-- pour toutes les actions de cet état
		for possible_action=0,(action_max-1),1 do
			possible_state_action=frame_alive+
				frame_max*(vic_x_i+
				x_max*(vic_y_i+
				y_max*possible_action));
			if not q_value[possible_state_action] then
				q_value[possible_state_action]=0;
			end
		end

		-- Update de la Q-Value
		q_value[state_action]=
			(1-1/a)*q_value[state_action]
			+(1/a)*(R+1*max_q_value);

		-- Update de l'état pour la prochaine boucle
		vic_x_i=vic_x;
		vic_y_i=vic_y;
		frame_alive = new_frame;
		state_i=frame_alive+frame_max*(vic_x_i+x_max*vic_y_i);	
	end

	-- update de la meilleur policy
	qvalue_max={};
	qvalue_argmax={};
	for key,value in pairs(q_value) do
		action=math.floor(key/(frame_max*x_max*y_max));
		vic_y=math.floor(key/(frame_max*x_max))-y_max*action;
		vic_x=math.floor(key/(frame_max))-x_max*(vic_y+y_max*action);
		frame=key-frame_max*(vic_x+x_max*(vic_y+y_max*action));
		state=frame+frame_max*(vic_x+x_max*vic_y);
		if not qvalue_max[state] then
			qvalue_max[state]=value;
			qvalue_argmax[state]=action;
		elseif value > qvalue_max[state] then
			qvalue_max[state]=value;
			qvalue_argmax[state]=action;
		end
	end
	
	for key,value in pairs(qvalue_max) do
		previous_action=0;
		if policy[key] then
			previous_action=policy[key];
		end
		previous_state_action=key
			+previous_action*frame_max*x_max*y_max;
		
		if q_value[previous_state_action]<value then
			policy[key]=qvalue_argmax[key];
		end
	end
end
