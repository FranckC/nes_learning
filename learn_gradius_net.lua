-- Launch ROM then this script.

-- Make sure the following packages are available.
require 'torch';
require 'nn';
require 'net-toolkit';

-- Set to true to start from scratch
-- or false to load a neural network saved to file
start_new=false;

----------------------------------------
-- -- HERE YOU CAN TUNE PARAMETERS -- --
----------------------------------------
-- action_max: number of inputs allowed
-- For gradius we chose 20 for:
-- (4 diagonal diirections+no directions)*(pressing A, A+B,B, or nothing)
-- If you change this value, you have to change the mapping between the
-- number of the action and the true input in the main loop.
action_max = 20; 

-- How much frames between every input.
-- 24 is 150APM or about 2 inputs/sec
input_frame_space=12;

-- How much inputs between 2 savestate (== a "segment")
segment_inputs=100;

-- Random exploration ratio (epsilon*100 % of
-- the inputs are random)
epsilon_max=1;

-- Maybe you want to change this value if you use another game engine
-- or if you want to use only some ranges in the RAM.
-- In this case you need to change the definition of the RAM
-- in the main loop.
RAM_size=2048;

-- RAM_size = RAM_size/RAM_norm for normalization purpose.
RAM_norm=256;

-- How many of the last inputs are stored in memory for learning
memory_size=math.floor((1.5)*segment_inputs);

-- How many frames is a state
memory_frames=4;

-- How many of those last inputs are used for learning after every input
minibatch_size=memory_size/30;

-- Preference for the future in (0,1]
gamma=0.8;

-- A safe buffer between 2 states 
-- so that the game doesn't load in an
-- environment impossible to solve.
-- This parameter doesn't need tuning.
caution_ratio=2;

-- Emulation speed ("maximum","turbo" or "normal")
-- Choose the fastest one, however the bottleneck for performance
-- is in the back propagation, not the emulation.
emu.speedmode("maximum");

-- HERE: load or define the new neural network
if start_new then
-- Define neural network
	cnn=nn.Sequential();
	cnn:add(nn.TemporalConvolution(memory_frames,16,64,4));
	cnn:add(nn.ReLU());
	cnn:add(nn.TemporalConvolution(16,32,16,2));
	cnn:add(nn.ReLU());
	cnn:add(nn.View(7712));
	cnn:add(nn.Linear(7712,256));
	cnn:add(nn.ReLU());
	cnn:add(nn.Linear(256,action_max));
else
	cnn, w, dw = netToolkit.loadNet("./vic.net");
end

criterion=nn.MSECriterion();
trainer=nn.StochasticGradient(cnn,criterion);
trainer.maxIteration=1;

-- You might also want to tune the reward
-- in the main loop
-----------------------------------
-- -- END OF PARAMETER TUNING -- --
-----------------------------------

-- Number of frames between two savestates
segment_frames=input_frame_space*segment_inputs;

-- Useful input
pressStart={
	up=false, down=false, 
	left=false, right=false,
	A=false, B=false,
	start=true, select=false
	};

-- Open the file to remember the score at each iteration
-- (plot it to visualize how the learning process is going)
score_history=io.open('score_history.vic','w');

-- Initialize a random seed
math.randomseed(os.time());

-- Initialize savestates
slot1=savestate.object(1);
slot2=savestate.object(2);

-- Initialize the memory of the agent"
mem_states=torch.zeros(RAM_size,memory_size);
mem_is_dead=torch.zeros(memory_size);
mem_action=torch.zeros(memory_size);
mem_reward=torch.zeros(memory_size);

-- Reset
emu.softreset();

-- Two following blocks are Gradius specific:
-- Press start 2 times for 1 player mode
emu.frameadvance();
emu.frameadvance();
emu.frameadvance();
emu.frameadvance();
joypad.set(1, pressStart);
emu.frameadvance();
emu.frameadvance();
joypad.set(1, pressStart);
-- Pass 2sc where nothing happens
for t=1,120,1 do
	emu.frameadvance();
end

-- Wait for the instance to be created
while not isAlive  do
	emu.frameadvance();
	isAlive=memory.readbyte(0x0100)==1;
end

-- Initialization of the savestates
savestate.save(slot1);
savestate.persist(slot1);
savestate.save(slot2);
savestate.persist(slot2);
last_frame_save=emu.framecount();
backup_frame_save=last_frame_save;
frame_save1=last_frame_save;
frame_save2=last_frame_save;
last_is_1=true;
frame_alive=last_frame_save;

-- Intialization of a pointer in the memory
mem_i=1;
for i=1,memory_frames,1 do
	mem_is_dead[1+(mem_i-1+i-1)%memory_size]=1;
end

while true do

	-- Say if vic is dead to fast after the last savestate
	-- it means the savestate before have to be loaded
	dead_fast=(frame_alive-last_frame_save-input_frame_space)<(segment_frames/caution_ratio);
	
	if dead_fast then
		cautious_slot=not last_is_1;
	else
		cautious_slot=last_is_1;
	end
				
	if cautious_slot then
		savestate.load(slot1);
		frame_alive=frame_save1;
	else
		savestate.load(slot2);
		frame_alive=frame_save2;
	end
	
	-- Count the number of inputs from this save
	mem_i_save=mem_i;
	
	-- Load the RAM into a tensor into the memory
	-- how to use memory.readrange ?
	state_RAM=torch.zeros(RAM_size,1);
	for addr=1,RAM_size,1 do
		state_RAM[addr]=(memory.readbyte(addr-1))/RAM_norm;
	end
	mem_states[{{},1+((mem_i-1)%memory_size)}]=state_RAM;
	
	-- Compute the score
	score_low=memory.readbyte(0x07E4);
	score_med=memory.readbyte(0x07E5);
	score_high=memory.readbyte(0x07E6);
	unit=score_low%16;
	dec=math.floor(score_low/16);
	hun=score_med%16;
	tho=math.floor(score_med/16);
	ten=score_high%16;
	cen=math.floor(score_high/16);
	score=unit+10*dec+100*hun+1000*tho+10000*ten+10000*cen;

	isAlive=true;
	
	-- Exploration ratio for this run
	epsilon=math.random()*epsilon_max;

	-- Until game is lost, do:
	while isAlive do
	
		-- create new savestate if the current frame is far enough
		if (frame_alive-last_frame_save)>(segment_frames-1) then
			if last_is_1 then
				savestate.save(slot2);
				savestate.persist(slot2);
				frame_save2=frame_alive;
				last_frame_save=frame_save2;
				backup_frame_save=frame_save1;
			else
				savestate.save(slot1);
				savestate.persist(slot1);
				frame_save1=frame_alive;
				last_frame_save=frame_save1;
				backup_frame_save=frame_save2;
			end
			last_is_1 = not last_is_1;
		end

		-- switch back to last savestate if dead too fast
		if (deadfast 
			and (frame_alive-backup_frame_save)>(segment_frames-1)
			and (frame_alive-backup_frame_save)<(segment_frames+input_frame_space)) 
		then
			if last_is_1 then
				savestate.save(slot1);
				savestate.persist(slot1);
				frame_save1=frame_alive;
				last_frame_save=frame_save1;
			else
				savestate.save(slot2);
				savestate.persist(slot2);
				frame_save2=frame_alive;
				last_frame_save=frame_save2;
			end
		end

		-- Choose action in accordance with the exploration policy
		exploration=((math.random()<epsilon) or (mem_i<=(memory_size+1)) or (mem_i<(mem_i_save+memory_frames-1)));
		if exploration then
			action=math.random(1,action_max);
		else
			-- take the action that maximize
			-- the prediction of the Q value for the algorithm.
			-- Q value is a score over all possible action
			-- for a given state.
			state=torch.zeros(RAM_size,memory_frames);
			for i=1,memory_frames,1 do
				state[{{},i}]=mem_states[{{},1+((mem_i-1-(memory_frames-i))%memory_size)}];
			end
			q=cnn:forward(state);
			_, action=torch.max(q,1);
			action=action[1];
		end
		action=action-1;
		
		-- Define the inputs here
		-- You might want to change that depending on the game/engine
		dir_action=action%5;
		if dir_action==0 then
			go_up=false;
			go_left=false;
			go_down=false;
			go_right=false;
		else
			go_up= (dir_action==1)or(dir_action==2);	
			go_left=(dir_action==1)or(dir_action==3);
			go_down=not go_up;
			go_right=not go_left;
		end
		A_action=math.floor(action/10);
		press_A=(A_action==0);
		B_action=math.floor((action%10)/5);
		press_B=(B_action==0);

		inputs={
			up= go_up, down= go_down,
			left=go_left, right=go_right,
			A=press_A, B=press_B, 
			start=false, select=false
			};
				
		-- Load inputs
		for frame=1,input_frame_space,1 do
			joypad.set(1,inputs);
			emu.frameadvance();
		end
		
		-- get new state
		state_RAM_new=torch.zeros(RAM_size,1);
		for addr=1,RAM_size,1 do
			state_RAM_new[addr]=(memory.readbyte(addr-1))/RAM_norm;
		end
		
		isAlive=memory.readbyte(0x0100)==1;

		-- Define the reward
		score_low=memory.readbyte(0x07E4);
		score_med=memory.readbyte(0x07E5);
		score_high=memory.readbyte(0x07E6);
		unit=score_low%16;
		dec=math.floor(score_low/16);
		hun=score_med%16;
		tho=math.floor(score_med/16);
		ten=score_high%16;
		cen=math.floor(score_high/16);
		score_new=unit+10*dec+100*hun+1000*tho+10000*ten+10000*cen;
		reward=(score_new-score)+0.05;
		if not isAlive then
			reward=-10;
		end
		reward=5*reward;

		-- Add the new state in the memory
		mem_action[1+((mem_i-1)%memory_size)]=action;
		mem_reward[1+((mem_i-1)%memory_size)]=reward;
		mem_i=mem_i+1;
		mem_states[{{},1+((mem_i-1)%memory_size)}]=state_RAM_new;
		
		-- To avoid bad transition because of too fast death
		if (not isAlive) and (mem_i<(mem_i_save+memory_frames)) then
			mem_i=mem_i_save;
		elseif isAlive then
			mem_is_dead[1+((mem_i-1)%memory_size)]=0;
		else
			for i=1,memory_frames,1 do
				mem_is_dead[1+((mem_i-1+i-1)%memory_size)]=1;
			end
		end

		-- When the experience is big enough, we can learn.
		if mem_i>memory_size then
		
			-- Take random minibatch of the transitions in memory
			-- including the very last transition
			-- and make sure no bad transitions because of deaths
			-- are taken
			minibatch_index=torch.zeros(memory_size);
			minibatch_index[1+((mem_i-2)%memory_size)]=1;
			for sample=2,minibatch_size,1 do
				index=math.random(1,memory_size);
				while ((minibatch_index[1+((index-1)%memory_size)]~=0)
					or (mem_is_dead[1+((index-1)%memory_size)]~=0))
				 do
					index=index+1;
				end
				minibatch_index[1+((index-1)%memory_size)]=1;
			end

			-- Create the dataset
			dataset={};
			function dataset:size() return minibatch_size end
			index=0;
			for i=1,memory_size,1 do
				if (minibatch_index[i]==1) then
					input=torch.zeros(RAM_size,memory_frames);
					for j=1,memory_frames,1 do
						input[{{},j}]=mem_states[{{},1+((i-1-(memory_frames-j))%memory_size)}];
					end
					action_t=mem_action[i];
					reward_t=mem_reward[i];
					v=cnn:forward(input);
					output=v:clone();
					if reward_t <0 then
						output[action_t+1]=reward_t;
					else
						state_new=torch.zeros(RAM_size,memory_frames);
						for j=1,memory_frames,1 do
							state_new[{{},j}]=mem_states[{{},1+((i-(memory_frames-j))%memory_size)}];
						end
						q_t=cnn:forward(state_new);
						q_t_max=torch.max(q_t);
						output[action_t+1]=reward_t+gamma*q_t_max;
					end
					index=index+1;
					dataset[index]={input,output};
				end
			end
			
			-- Gradient descent step
			trainer:train(dataset);
			
			-- save the score if dead
			if not isAlive then
				score_history:write(score);
				score_history:write('\n');
			end
		end

		-- pass variables
		state_RAM=state_RAM_new;
		score=score_new;
		frame_alive=emu.framecount();

		-- save every x inputs
		if (mem_i>memory_size) and (mem_i%memory_size)==1 then
			w, dw=netToolkit.saveNet("./vic.net",cnn);
			score_history:flush();
		end
	end	
end
