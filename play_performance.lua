-- Lancer la rom, puis lancer ce script
-- avec le GUI de FCEUX à la main.
-- Ensuite regarder.

-- Vitesse de l'émulation
emu.speedmode('normal');

-- Touches utiles:
pressStart={
	up=false,down=false,
	left=false,right=false,
	A=false,B=false,
	start=true,select=false
	};
	
releasePad={
	up=false,down=false,
	left=false,right=false,
	A=false,B=false,
	start=false,select=false
	};
	
while true do

	-- Reset de la console
	emu.softreset();

	-- Appuyer deux fois sur start pour lancer le mode 1J
	emu.frameadvance();
	emu.frameadvance();
	emu.frameadvance();
	emu.frameadvance();
	joypad.set(1,pressStart);
	emu.frameadvance();
	emu.frameadvance();
	joypad.set(1,pressStart);

	-- Attendre que l'instance de jeu soit créee
	-- (i.e attendre que le vaisseau existe)
	while not isAlive do
		emu.frameadvance();
		isAlive=memory.readbyte(0x0100)==1;
	end

	-- Chargement des inputs
	inputs_script=io.lines('best_time.history');
	
	i=0;
	
	for eachline in inputs_script do
		action=tonumber(eachline);
		if i>1 then
			-- Définition des inputs à cette frame
			if action==0 then
				inputs=releasePad;
			else
				go_up=(action==1)or(action==2);
				go_left=(action==1)or(action==3);
				press_A=false;
				press_B=false;
	
				-- Mise sous forme acceptée par l'émulateur
				inputs={
					up=go_up, down=not go_up,
					left=go_left, right=not go_left,
					A=press_A, B=press_B,
					start=false, select=false
					};
			end

			-- saisie des inputs pendant x frames
			-- (must match training input rate)
			for frame=1,input_frame_space,1 do
				joypad.set(1,inputs);
				emu.frameadvance();
			end
		elseif i==0 then
			emu.message(action);
			i=1;
		elseif i==1 then
			input_frame_space=action;
			i=2;
		end
	end
end
