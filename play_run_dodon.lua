-- Lancer la rom, puis lancer ce script
-- avec le GUI de FCEUX à la main.
-- Ensuite regarder.

-- Vitesse de l'émulation
emu.speedmode('normal');
shoot_framerate=12;

while true do

	-- Chargement des inputs
	inputs_script=io.lines('best_frame.input');
	
	i=0;
		
	for eachline in inputs_script do
		action=tonumber(eachline);
		if i>0 then
			-- Définition des inputs à cette frame
			if action==0 then
				inputs={["P1 Button 1"]=true};
				for frame=1,input_frame_space,1 do
					inputs_shoot={["P1 Button 1"]=true};
					if ((frame%shoot_framerate)<6) then
					joypad.set(inputs)
				end
				emu.frameadvance();
			end
			else
				go_up=(action==1)or(action==2);
				go_left=(action==1)or(action==3);
	
				-- Mise sous forme acceptée par l'émulateur
				inputs_noshoot={["P1 Up"]=go_up, ["P1 Down"]=not go_up,
					["P1 Left"]=go_left, ["P1 Right"]=not go_left
					};
				inputs_shoot={["P1 Up"]= go_up, ["P1 Down"]= not go_up,
					["P1 Left"]=go_left, ["P1 Right"]=not go_left,
					["P1 Button 1"]=true
					};
					
				for frame=1, input_frame_space,1 do
					if ((frame%shoot_framerate)<6) then
						joypad.set(inputs_shoot);
					else
						joypad.set(inputs_noshoot);
					end
					emu.frameadvance();
				end
			end
		elseif i==0 then
			input_frame_space=action;
			i=1;
		end
	end
end
